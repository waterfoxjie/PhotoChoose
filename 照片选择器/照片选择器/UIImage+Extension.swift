//
//  UIImage+Extension.swift
//  照片选择器
//
//  Created by waterfoxjie on 15/8/18.
//  Copyright © 2015年 zj. All rights reserved.
//

import UIKit

/*
需求：图像等比例缩放，有一个统一的缩放宽度

*/

extension UIImage {

    // 缩放图像的函数
    func scallImage(width : CGFloat) -> UIImage {
    
        // 如果图像本身很小的画，就直接返回
        if size.width < width {
        
            return self
        }
        
        // MARK: - 使用图像上下文重新绘制图像
        // 1、计算目标尺寸（等比例缩放）
        let height = size.height * width / size.width
        
        let s = CGSize(width: width, height: height)
        
        // 2、开启图像上下文
        UIGraphicsBeginImageContext(s)
        
        // 3、绘图
        drawInRect(CGRect(origin: CGPointZero, size: s))
        
        // 从当前上下文拿到结果
        let result = UIGraphicsGetImageFromCurrentImageContext()
        
        // 关闭图像上下文
        UIGraphicsEndImageContext()
        
        return result
        
    }
    
}
